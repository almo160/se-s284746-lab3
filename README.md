# se-s284746-lab3
This is a deployed Lab 3 on Software Engineering
## Author
P33212 Evgeny Ivanov @AXER-doc
## High-Level Overview
This application checks if the user defined point belongs to the area (the area is visualized in the picture on the app interface) and saves results to PostgreSQL database.
## Details
- Programming language is Java
- Application uses Java Server Faces 2.0
- Application uses ICEfaces JSF Framework 4.3.0
- Database server is PostgreSQL 13, JDBC driver is org.postgresql:postgresql:jar:42.2.20
- Webapp container is Tomcat Embed 8.0.47